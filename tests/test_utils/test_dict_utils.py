import unittest

from src.utils.dict_utils import extract_attribute_values

dict = {
    'NumContrato': {
        'new_name': 'num_contrato',
        'formatter': None,
        'required': True
    },
    'NumExpediente': {
        'new_name': 'num_expediente',
        'formatter': None,
        'required': False
    },
    'ObjetoContrato': {
        'new_name': 'objeto_contrato',
        'formatter': None,
        'required': True
    },
    'ObjetoNone': {
        'new_name': None,
        'formatter': None,
        'required': True
    }
}


class TestDictUtils(unittest.TestCase):

    def test_extract_attribute_values_empty(self):
        columns_info = {}
        attribute_name = "new_name"
        expected_result = {}
        self.assertEqual(extract_attribute_values(
            columns_info, attribute_name), expected_result)

    def test_extract_attribute_values(self):
        columns_info = dict
        attribute_name = "new_name"
        expected_result = {'NumContrato': 'num_contrato',
                           'NumExpediente': 'num_expediente',
                           'ObjetoContrato': 'objeto_contrato'}
        self.assertEqual(extract_attribute_values(
            columns_info, attribute_name), expected_result)


if __name__ == '__main__':
    unittest.main()
