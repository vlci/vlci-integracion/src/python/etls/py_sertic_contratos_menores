import unittest

import numpy as np
import pandas as pd

from src.config.columns_config import to_date, to_number, to_pg_bool


class TestParsingFunctions(unittest.TestCase):

    def test_to_number(self):
        self.assertAlmostEqual(to_number("3.14"), 3.14)
        assert pd.isna(to_number("abc"))
        assert pd.isna(to_number(""))  # Empty string
        assert pd.isna(to_number(None))  # None value

    def test_to_date(self):
        self.assertEqual(to_date("2023-12-15"), pd.Timestamp("2023-12-15"))
        self.assertEqual(to_date("2023-12-15T12:34:56"),
                         pd.Timestamp("2023-12-15"))
        assert pd.isna(to_date("abc"))
        assert pd.isna(to_date("abc"))
        assert pd.isna(to_date(""))  # Empty string
        assert pd.isna(to_date(None))  # None value

    def test_to_pg_bool(self):
        self.assertEqual(to_pg_bool("true"), "true")
        self.assertEqual(to_pg_bool("false"), "false")
        self.assertEqual(to_pg_bool("invalid"), "NULL")
        self.assertEqual(to_pg_bool(""), "NULL")
        self.assertEqual(to_pg_bool(None), "NULL")


if __name__ == '__main__':
    unittest.main()
