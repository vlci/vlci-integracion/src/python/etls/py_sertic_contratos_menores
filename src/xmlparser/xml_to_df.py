import pandas as pd


def parse(xml: str, config: dict) -> pd.DataFrame:
    '''Método para leer un xml en el que la información necesaria
    de los nodos que representan entidades está toda en el primer nivel dentro
    de dicho nodo'''
    try:
        df_contr_adj = pd.read_xml(path_or_buffer=xml,
                                   namespaces=config['result_namespaces'],
                                   xpath=config['xpath'],
                                   converters=config['converters'],
                                   parser='lxml')
    except ValueError:
        return pd.DataFrame()

    return df_contr_adj
