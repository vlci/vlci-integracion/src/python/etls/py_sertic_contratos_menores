contr_request_namespaces = {
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
    'wsu': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
    'v20': 'http://www.valencia.es/services/esb/ContratosMenores/v20',
    'con': 'http://www.valencia.es/services/ContratosMenores',
    'com': 'http://www.valencia.es/schema/ContratosMenores/common'
}

auth_fields = {
    'username': './/wsse:Username',
    'password': './/wsse:Password'
}

date_fields = {
    'start_date': './/com:Desde',
    'finish_date': './/com:Hasta'
}

contr_namespaces = {
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'v20': 'http://www.valencia.es/services/esb/ContratosMenores/v20',
    'ns4': 'http://www.valencia.es/schema/ContratosMenores/common',
    'doc': 'http://www.valencia.es/services/ContratosMenores'
}
