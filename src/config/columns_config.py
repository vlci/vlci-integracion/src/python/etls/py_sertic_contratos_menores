import numpy as np
import pandas as pd


def to_number(value: str) -> float:
    try:
        return pd.to_numeric(value, errors='coerce')
    except Exception:
        return np.NaN


def to_date(value: str) -> pd.Timestamp:
    try:
        return pd.to_datetime(value[0:10], format="%Y-%m-%d", errors='coerce')
    except Exception:
        return pd.NaT


def to_pg_bool(value: str) -> str:
    if value == 'true':
        return 'true'
    elif value == 'false':
        return 'false'
    else:
        return 'NULL'


# El nombre del usuario que va a usarse para insertar/actualizar
user_insert = 'aud_user_ins'
user_updt = 'aud_user_upd'

# El nombre de la columna que va a tener el código de unidades administrativas
code_column = 'unidad_administrativa'
# El nombre de la columna de la que se saca el código de unidad administrativa
code_origin = 'DescripcionUnidad'


columns_config = {
    'NumContrato': {
        'new_name': 'num_contrato',
        'formatter': None,
        'required': True},
    'NumExpediente': {
        'new_name': 'num_expediente',
        'formatter': None,
        'required': True},
    'ObjetoContrato': {
        'new_name': 'objeto_contrato',
        'formatter': None,
        'required': True},
    'FechaAdjudicacion': {
        'new_name': 'fecha_adjudicacion',
        'formatter': to_date,
        'required': True},
    'CentroGasto': {
        'new_name': 'centro_gastos',
        'formatter': None,
        'required': False},
    'AplicPresupuestaria': {
        'new_name': 'aplicacion_presupuestaria',
        'formatter': None,
        'required': False},
    'TipoContrato': {
        'new_name': 'tipo_contrato',
        'formatter': None,
        'required': False},
    'ImporteSinIVA': {
        'new_name': 'importe_sin_iva',
        'formatter': to_number,
        'required': False},
    'ImporteIVA': {
        'new_name': 'iva',
        'formatter': to_number,
        'required': False},
    'OrganoContratacion': {
        'new_name': 'organo_contratacion',
        'formatter': None,
        'required': False},
    'DescripcionUnidad': {
        'new_name': 'unidad_tramitadora',
        'formatter': None,
        'required': True},
    'NumPropuesta': {
        'new_name': 'numero_propuestas',
        'formatter': to_number,
        'required': False},
    'NumeroOfertas': {
        'new_name': 'numero_ofertas',
        'formatter': to_number,
        'required': False},
    'NombreAdjudicatario': {
        'new_name': 'adjudicatario',
        'formatter': None,
        'required': False},
    'Estado': {
        'new_name': 'estado',
        'formatter': None,
        'required': True},
    'FechaInicio': {
        'new_name': 'fecha_inicio_contrato',
        'formatter': to_date,
        'required': False},
    'FechaFin': {
        'new_name': 'fecha_fin_contrato',
        'formatter': to_date,
        'required': False},
    'DuracionContrato': {
        'new_name': 'duracion_contrato_meses',
        'formatter': to_number,
        'required': False},
    'FechaFactura': {
        'new_name': 'fecha_factura',
        'formatter': to_date,
        'required': False},
    'FechaPago': {
        'new_name': 'fecha_pago',
        'formatter': to_date,
        'required': False},
    'VariasOfertas': {
        'new_name': 'varias_ofertas',
        'formatter': to_pg_bool,
        'required': False},
    'AcuerdoMarco': {
        'new_name': 'acuerdo_marco',
        'formatter': to_pg_bool,
        'required': False},
    'AnticipoCajaFija': {
        'new_name': 'anticipo_caja_fija',
        'formatter': to_pg_bool,
        'required': False},
    'Modificacion': {
        'new_name': 'modificacion',
        'formatter': to_pg_bool,
        'required': False},
    'InformeEstudio': {
        'new_name': 'informe_estudio',
        'formatter': to_pg_bool,
        'required': False},
    'FechaResolucionContrato': {
        'new_name': 'fecha_resolucion',
        'formatter': to_date,
        'required': False},
    'NumeroResolucionContrato': {
        'new_name': 'numero_resolucion',
        'formatter': None,
        'required': False},
    'UnidadAdministrativa': {
        'new_name': 'unidad_administrativa',
        'formatter': None,
        'required': True}
}


def get_required_columns(columns_config: dict) -> list:
    """
    Devuelve la lista de nombres de las columnas obligatorias
    """
    required_columns = []
    for config in columns_config.values():
        if config['required']:
            required_columns.append(config['new_name'])
    return required_columns
