import os


def split_into_number_array(property: str) -> list:
    string_list = property.split(',')
    number_list = []
    for entrie in string_list:
        number_list.append(int(entrie))
    return number_list


# Configuración ETL
EXEC_ENV = os.getenv('entorno_ejecucion', None)
ETL_NAME = os.getenv('etl_name', 'ETL_CONTRATACION_CONTRATOS_MENORES')
LOGING_LEVEL = os.getenv('loging_level', 'INFO')

LOGING_FOLDER = os.getenv('loging_folder', 'logs')
ERRORS_FOLDER = os.getenv('errors_folder', 'errors')
TMP_FOLDER = os.getenv('tmp_folder', 'tmp_folder')
ERRORS_FILE_PATTERN = os.getenv('errors_file_pattern', 'errors_DATE.csv')

# Configuración Email
MAIL_SENDER = os.getenv('mail_sender', None)
MAIL_SENDER_NAME = os.getenv('mail_sender_name', 'Equipo integración')
MAIL_HOST = os.getenv('global_smtp_host', None)
MAIL_PORT = os.getenv('global_smtp_port', None)
MAIL_USERNAME = os.getenv('global_smtp_username', None)
MAIL_PASSWORD = os.getenv('global_smtp_password', None)
MAIL_SEND_TO = os.getenv('mail_send_to', None)
MAIL_MESSAGE = os.getenv(
    'mail_message',
    '''El fichero errores.csv contiene información de los errores de validación obtenidos.
Analizar los errores de validación obtenidos y ver si efectivamente el servicio SOAP está devolviendo esos valores.
Para ello, utilizar el WS "findContratosPublicados" y buscar el contrato que ha fallado.
Si es un error en el servicio SOAP, enviar el listado de errores a V.Sancho.''')

# Configuración Postrgresql
POSTGRES_HOST = os.getenv('global_database_postgis_host_vlci2', None)
POSTGRES_PORT = os.getenv('global_database_postgis_port_vlci2', None)
POSTGRES_DATABASE = os.getenv('global_database_postgis_database_vlci2', None)
POSTGRES_USER = os.getenv('global_database_postgis_login_vlci2', None)
POSTGRES_PASS = os.getenv('global_database_postgis_password_vlci2', None)
POSTGRES_TABLE = os.getenv('postgis_table_vlci2',
                           't_datos_etl_contratos_menores')
POSTGRES_SCHEMA = os.getenv('postgres_schema_vlci2',
                            'vlci2')

# Configuración para SOAP service
CONTRATACION_SERVICE_URL = os.getenv('global_contratacion_service_url', None)
XLM_CONTR_ADJ_MODEL = os.getenv(
    'xml_contr_adj_model', 'templates/contr_adjs.xml')
SOAP_SERVICE_USER = os.getenv('global_soap_service_username', None)
SOAP_SERVICE_PASSWORD = os.getenv('global_soap_service_password', None)
SOAP_SERVICE_RETRIES = int(os.getenv('service_retries', 10))
SOAP_SERVICE_BACKOFF = int(os.getenv('service_backoff_factor', 1))
SOAP_SERVICE_CODES_RETRY = split_into_number_array(
    os.getenv('service_codes_retry', '429,500,502,503,504'))
SOAP_SERVICE_ALLOWED_METHODS = os.getenv('service_allowed_methods',
                                         'POST').split(',')

# Entidad que se desea parsear de la respuesta del servicio
ENTITY = os.getenv('entity', '//doc:ContratoMenor')
# Formato en el que vienen las fechas en el web service ignorando el timezone
SOAP_SERVICE_DATE_FORMAT = os.getenv('soap_service_date_format', "%Y-%m-%d")
# Regex del formato de código unidad administrativa
REGEX_UA_CODE = os.getenv('regex_ua_code', "([0-9a-zA-Z]{5})(?= - )")
# Regex de la descripción de unidad administrativa
REGEX_UA_SEPARATOR = os.getenv(
    'code_description_separator', "^(?:[0-9]{5} - )?(.*)")
# De cuantos días previos se quieren re procesar los contratos del servicio web
DAYS_TO_REPROCESS = int(os.getenv('days_to_reprocess', 60))

# Query para gestión de fechas
EXEC_DATE_QUERY = os.getenv(
    'query_fecha_ejecucion',
    '''select fen
    from  vlci2.t_d_fecha_negocio_etls
    where etl_nombre = \'py_contratacion_contratos_menores\'''')
STATE_QUERY = os.getenv(
    'query_estado',
    '''update vlci2.t_d_fecha_negocio_etls
        set estado = \'STATE\'
        where etl_nombre = \'py_contratacion_contratos_menores\'''')
DATE_QUERY = os.getenv(
    'query_fecha',
    '''update vlci2.t_d_fecha_negocio_etls
       set fen = \'DATE\'
       where etl_nombre = \'py_contratacion_contratos_menores\'''')

OK_STATE = os.getenv('ok_state', 'OK')
ONGOING_STATE = os.getenv('ongoing_state', 'EN PROCESO')
ERROR_STATE = os.getenv('error_state', 'ERROR')
