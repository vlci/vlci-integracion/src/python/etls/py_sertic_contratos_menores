from datetime import date

import pandas as pd


def write_errors(df: pd.DataFrame,
                 errors_folder: str,
                 pattern: str,
                 date_format: str
                 ) -> str:
    '''Escribe las entradas no validas en un csv y
    devuelve el nombre del fichero'''
    errors_filename =\
        f'{errors_folder}/{pattern}'.replace(
            'DATE',
            date.today().strftime(date_format))

    with open(errors_filename, 'w') as f:
        f.write(df.to_csv(index=False))

    return errors_filename
