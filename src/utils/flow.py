import logging
import sys
import traceback
from types import ModuleType

from sqlalchemy import Engine
from vlcishared.mail.mail import Email

from postgresql.alchemy import execute_update

NON_FATAL_ERROR = 2
FAILED_EXEC = 1
SUCCESS_EXEC = 0


class FlowControl:
    '''Clase que implementa métodos que se usan para controlar
    el flujo de la ETL, por ejemplo para terminar la ejecución,
    para permitir que continue pero que envie un correo al final
    o para indicar que la ejecución ha sido exitosa'''

    flow_state = None
    logger = logging.getLogger()

    def __init__(self,
                 config: ModuleType,
                 log: logging.Logger,
                 mail: Email,
                 db: Engine) -> None:
        self.flow_state = SUCCESS_EXEC
        self.log = log
        self.mail = mail
        self.db = db
        self.config = config

    def handle_error(self, cause: str, fatal: bool = False) -> None:
        '''Logea el error recibido, lo añade al correo que se va a enviar y
        si es un error fatal termina la ejecución de la ETL'''
        self.flow_state = FAILED_EXEC if fatal else NON_FATAL_ERROR
        self.log.error(cause)
        self.log.error(f'Excepción: {traceback.format_exc(limit=1)}')

        self.mail.append_line('Ejecución Fallida: ETL KO.')
        self.mail.append_line(f'Causa del fallo: {cause}')

        if fatal:
            self.end_exec()

    def end_exec(self, date: str = ''):
        '''Termina la ejecución de la ETL retornando 1 en caso de fallo y 0
        en caso se exito'''
        if self.flow_state == FAILED_EXEC:
            self.set_failed_status()
        else:
            self.set_ok_status(date)

        if self.flow_state != SUCCESS_EXEC:
            self.mail.append_line('Se adjuntan logs de la ejecución.')
            self.mail.add_attachment(
                self.log.handlers[0].baseFilename, 'text/plain')
            self.mail.send()
            self.log.info('Ejecución Fallida, ETL KO')
        else:
            self.log.info('Ejecución Exitosa, ETL OK')

        return sys.exit(self.flow_state)

    def set_ongoing_status(self):
        execute_update(self.db, self.config.STATE_QUERY.replace(
            'STATE', self.config.ONGOING_STATE))

    def set_ok_status(self, date):
        execute_update(self.db, self.config.DATE_QUERY.replace(
            'DATE', date))
        execute_update(self.db, self.config.STATE_QUERY.replace(
            'STATE', self.config.OK_STATE))

    def set_failed_status(self):
        execute_update(self.db, self.config.STATE_QUERY.replace(
            'STATE', self.config.ERROR_STATE))
