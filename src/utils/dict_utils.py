def extract_attribute_values(columns_info: dict, attribute_name: str) -> dict:
    '''De un diccionario donde los valores don también diccionarios
    (de un solo nivel) devuelve las llaves y los valores del atributo no nulos
    pasado por parámetro como diccionario llave: valor'''
    attribute_values_dict = {}
    for column, info in columns_info.items():
        if info.get(attribute_name) is not None:
            attribute_values_dict[column] = info.get(attribute_name)
    return attribute_values_dict
