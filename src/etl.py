import logging
import os
import warnings
from datetime import date, timedelta

from sqlalchemy import Engine
from vlcishared.logging.setup import log_setup
from vlcishared.mail.mail import Email

from bdo.soap import SoapRequest
from config import columns_config, config
from config.namespaces import (auth_fields, contr_namespaces,
                               contr_request_namespaces, date_fields)
from postgresql.alchemy import get_conn, get_db_date, upsert_df
from processing.dataframe import separate_valid_non_valid, transform_df
from utils.dict_utils import extract_attribute_values
from utils.files import write_errors
from utils.flow import FlowControl
from xmlparser.xml_to_df import parse

###################################################################
# contr = contratacion / contratos
# adj = adjudicados
###################################################################
warnings.filterwarnings("ignore")


def main():

    os.makedirs(config.LOGING_FOLDER, exist_ok=True)
    os.makedirs(config.ERRORS_FOLDER, exist_ok=True)
    log_setup(config.LOGING_FOLDER,
              config.ETL_NAME,
              config.LOGING_LEVEL)
    log = logging.getLogger()
    db = get_conn(config.POSTGRES_HOST,
                  config.POSTGRES_PORT,
                  config.POSTGRES_DATABASE,
                  config.POSTGRES_USER,
                  config.POSTGRES_PASS)
    mail = setup_mail()

    flow = FlowControl(config, log, mail, db)
    log.info('Iniciando ejecución ETL')

    try:
        start_date = get_start_date(db)
        end_date = get_end_date()
        flow.set_ongoing_status()
        log.info(f'Fecha de inicio de datos: {start_date}'
                 f', fecha fin: {end_date}')

        contr_adj_config = {
            'req_model': config.XLM_CONTR_ADJ_MODEL,
            'req_namespaces': contr_request_namespaces,
            'fields': [auth_fields, date_fields],
            'update_fields': {
                'username': config.SOAP_SERVICE_USER,
                'password': config.SOAP_SERVICE_PASSWORD,
                'start_date': start_date,
                'finish_date': end_date
            },
            'service_url': config.CONTRATACION_SERVICE_URL
        }

        request_handler = SoapRequest(config.SOAP_SERVICE_RETRIES,
                                      config.SOAP_SERVICE_BACKOFF,
                                      config.SOAP_SERVICE_CODES_RETRY,
                                      config.SOAP_SERVICE_ALLOWED_METHODS)
        contr_response = request_handler.get_data_from_soap(contr_adj_config)

        parse_response_config = {
            'result_namespaces': contr_namespaces,
            'converters': extract_attribute_values(
                columns_config.columns_config,
                'formatter'
            ),
            'xpath': config.ENTITY
        }

        df_all_contracts = parse(contr_response, parse_response_config)

        if not df_all_contracts.empty:
            df_all_contracts = transform_df(df_all_contracts,
                                            columns_config,
                                            config)

            df_valid_contracts, df_invalid_contracts = (
                separate_valid_non_valid(df_all_contracts, columns_config))

            log.info(f'Se han recibido {df_all_contracts.size} contratos.')
            log.info(f'Cantidad de contratos válidos '
                     f'{df_valid_contracts.size}')
            log.info(f'Cantidad de contratos inválidos: '
                     f'{df_invalid_contracts.size}')

            del df_all_contracts

            if not df_invalid_contracts.empty:
                errors_filename = write_errors(df_invalid_contracts,
                                               config.ERRORS_FOLDER,
                                               config.ERRORS_FILE_PATTERN,
                                               config.SOAP_SERVICE_DATE_FORMAT)
                log.info(f'Guardando errores en {errors_filename}')
                mail.append_line('Se adjunta el fichero de errores')
                mail.append_line(config.MAIL_MESSAGE)
                mail.add_attachment(errors_filename, 'text/plain')
                flow.handle_error(cause='Hay contratos no válidos',
                                  fatal=False)

            df_valid_contracts.to_sql(name=config.POSTGRES_TABLE,
                                      schema=config.POSTGRES_SCHEMA,
                                      con=db,
                                      index=False,
                                      if_exists='append',
                                      method=upsert_df)

        else:
            log.info('No se ha recibido ningún contrato del servicio SOAP')

    except Exception as e:
        flow.handle_error(cause=f'Fallo En la ejecución de la ETL, causa: {e}',
                          fatal=True)

    flow.end_exec(end_date)


def setup_mail():
    mail = Email(config.MAIL_HOST,
                 config.MAIL_PORT,
                 config.MAIL_USERNAME,
                 config.MAIL_PASSWORD)
    mail.create_header(config.MAIL_SENDER,
                       config.MAIL_SENDER_NAME,
                       config.MAIL_SEND_TO,
                       f'[SmartCity]{config.EXEC_ENV} '
                       f'Ejecución {config.ETL_NAME}')
    mail.append_line('Resultado de la ejecución:')

    return mail


def get_start_date(db: Engine) -> str:
    retreived_date = get_db_date(db, config.EXEC_DATE_QUERY)
    if retreived_date is not None:
        start = (retreived_date -
                 timedelta(days=config.DAYS_TO_REPROCESS))
        start = start.strftime(config.SOAP_SERVICE_DATE_FORMAT)
    else:
        raise ValueError('No se pudo recuperar fecha de última ejecución')
    return start


def get_end_date() -> str:
    end_date = date.today() - timedelta(days=1)
    return end_date.strftime(config.SOAP_SERVICE_DATE_FORMAT)


if __name__ == '__main__':
    main()
