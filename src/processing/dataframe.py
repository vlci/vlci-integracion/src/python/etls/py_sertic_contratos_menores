from types import ModuleType

import pandas as pd

from utils.dict_utils import extract_attribute_values


def transform_df(df: pd.DataFrame,
                 col_config: ModuleType,
                 config: ModuleType) -> pd.DataFrame:
    '''Esta función realiza las transformaciones necesarias
    al DataFrame recibido para que el resultado pueda ser insertado
    en base de datos con poco procesamiento más.
    - De las columnas recibidas se queda con las que necesitamos
    - Crea la columna con el código de unidad administrativa
    - Crea las columnas de usuario para auditoria
    - Renombra todas las columnas de los nombres que vienen del web
     service a los nombres de las columnas en BD'''
    received_columns = set(df.columns)
    useful_columns = col_config.columns_config.keys()
    # AND lógico, efectivamente es un inner join
    use_columns = list(received_columns & useful_columns)
    result = df[use_columns]

    result[col_config.code_column] = \
        result[col_config.code_origin].str.extract(config.REGEX_UA_CODE)
    result[col_config.code_origin] = \
        result[col_config.code_origin].str.extract(
            config.REGEX_UA_SEPARATOR)

    result[col_config.user_updt] = config.ETL_NAME
    result[col_config.user_insert] = config.ETL_NAME

    renames = extract_attribute_values(
        col_config.columns_config,
        'new_name')

    result.rename(
        columns=renames,
        inplace=True,
        errors='ignore')
    return result


def separate_valid_non_valid(
        df: pd.DataFrame,
        config: ModuleType) -> tuple:
    '''Separa las entradas del dataframe validas de las no válidas
    devuelve una tupla donde la primera entrada son las válidas y
    la segunda las no válidas'''
    required_columns = config.get_required_columns(config.columns_config)
    df_valid_contracts = df.dropna(subset=required_columns,
                                   how='any',
                                   inplace=False)
    df_invalid_contracts = pd.merge(
        df,
        df_valid_contracts,
        indicator=True,
        how='outer').query(
            '_merge=="left_only"').drop(
                '_merge',
                axis=1)

    return df_valid_contracts, df_invalid_contracts
