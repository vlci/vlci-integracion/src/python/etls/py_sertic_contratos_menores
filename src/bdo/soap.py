import logging
import xml.etree.ElementTree as ET

from requests import Session
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


class ReqTemplate:

    def __init__(self,
                 xml_template: str,
                 namespaces: dict,
                 fields) -> None:
        self.tree = ET.parse(xml_template)
        self.root = self.tree.getroot()
        self.namespaces = namespaces

        if isinstance(fields, dict):
            self.fields = fields
        elif isinstance(fields, list):
            self.fields = {}
            for set in fields:
                self.fields.update(set)
        else:
            raise ValueError(
                f'fields solo puede ser un diccionario o'
                f'una lista de diccionarios')

    def __str__(self) -> str:
        return ET.tostring(self.tree.getroot(), encoding="unicode")

    def update_fields(self, **kwargs) -> None:
        for key, value in kwargs.items():
            field = self.root.find(self.fields[key], self.namespaces)
            if field is not None:
                field.text = value


class CustomRetry(Retry):
    log = logging.getLogger()

    def increment(self, *args, **kwargs):
        # Log each retry attempt
        self.log.info(
            f"Fallo en la consulta, intentos restantes: {self.total - 1}")
        return super().increment(*args, **kwargs)


class SoapRequest:

    def __init__(self,
                 retries: int,
                 backoff_factor: int,
                 codes_to_retry: list,
                 allowed_methods: list) -> None:
        self.log = logging.getLogger()
        retry_strategy = CustomRetry(
            total=retries,
            backoff_factor=backoff_factor,
            status_forcelist=codes_to_retry,
            method_whitelist=allowed_methods,
            raise_on_status=True
        )

        adapter = HTTPAdapter(max_retries=retry_strategy)

        self.session = Session()
        self.session.mount("http://", adapter)
        self.session.mount("https://", adapter)

    def __del__(self):
        try:
            self.session.close()
        except Exception:
            self.log.info('Error cerrando sesión para consultar el SOAP')

    def get_data_from_soap(self, config: dict) -> str:
        '''Pide datos del servicio soap para el que se configure.
            :param dict config: El objeto config debe contener:
            - req_model: la ruta al xml que se va a usar para hacer la peticion
            - req_namespaces: los namespaces de req_model
            - fields: la ruta xpath a los campos que se pretenden actualizar
            - update_fields: los nombres de los campos a actualizar y el valor
            - service_url: la url de la request
        '''
        contr_body = ReqTemplate(
            config['req_model'],
            config['req_namespaces'],
            config['fields']
        )

        contr_body.update_fields(
            **config['update_fields']
        )

        result = self.session.post(url=config['service_url'],
                                   data=str(contr_body),
                                   verify=False)

        if result.status_code != 200:
            raise ConnectionError(
                f'Fallo en la consulta al web service: {result.reason}, '
                f'code: {result.status_code}')
        return result.text
