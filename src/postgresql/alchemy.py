from datetime import date
from typing import Union

from sqlalchemy import Engine, create_engine, text
from sqlalchemy.dialects.postgresql import insert


def get_conn(host: str,
             port: str,
             database: str,
             user: str,
             password: str):
    try:
        return create_engine(
            f"postgresql://{user}:{password}@{host}:{port}/{database}")
    except Exception as e:
        raise ConnectionRefusedError(
            f"Error conectando a la base de datos: {e}")


def upsert_df(table, conn, keys, data_iter):
    '''Esta función es un callback que usa DataFrame.to_sql'''
    insert_stmt = insert(table.table).values(list(data_iter))

    set_on_duplicate = {}
    for key in keys:
        set_on_duplicate[key] = insert_stmt.excluded[key]

    on_duplicate_key_stmt = insert_stmt.on_conflict_do_update(
        index_elements=['num_contrato'],
        set_=set_on_duplicate
    )

    conn.execute(on_duplicate_key_stmt)


def get_db_date(engine: Engine, query: str) -> Union[date, None]:
    with engine.connect() as conn:
        date = conn.execute(text(query))
        row = date.fetchone()
        if row is not None:
            return row[0].date()


def execute_update(engine: Engine, query: str):
    with engine.connect() as conn:
        conn.execute(text(query))
        conn.commit()
